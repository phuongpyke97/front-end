var path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: './src/main.js', // sẽ chạy lần đầu tiên
  output: { // đóng gói và xuất ra ở đâu 
    path: path.resolve(__dirname, './dist'), // tên của thư mục khi build
    publicPath: '/dist/', // đường dẫn sẽ đóng gói ra ngoài
    filename: 'build.js' // tên của file build
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      }, {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader', // công cụ đóng gói hình ảnh 
        options: { // nơi để định nghĩa thư mục trong dist
          // name: '[name].[ext]?[hash]'
          name: function (file) {
            return 'assets/[name].[ext]?[hash]';
          }
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
