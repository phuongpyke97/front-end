import Vue from 'vue'
import App from './App.vue'
import 'bootstrap'
import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'popper.js/dist/popper.min.js';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserSecret)
import './assets/css/sb-admin-2.css';

Vue.component('font-awesome-icon', FontAwesomeIcon)
new Vue({
  el: '#app',
  render: h => h(App)
})
